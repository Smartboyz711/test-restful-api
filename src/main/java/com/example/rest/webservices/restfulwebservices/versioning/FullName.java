package com.example.rest.webservices.restfulwebservices.versioning;

public class FullName {
	
	private String firstName;
	
	private String lastname;
	
	public FullName() {
		super();
	}

	public FullName(String firstName, String lastname) {
		super();
		this.firstName = firstName;
		this.lastname = lastname;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	
}
