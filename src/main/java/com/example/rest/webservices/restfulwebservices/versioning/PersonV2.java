package com.example.rest.webservices.restfulwebservices.versioning;

public class PersonV2 {
	
	private FullName fullname;
	
	public PersonV2() {
		super();
	}

	public PersonV2(FullName fullname) {
		super();
		this.fullname = fullname;
	}

	public FullName getFullname() {
		return fullname;
	}

	public void setFullname(FullName fullname) {
		this.fullname = fullname;
	}
	
	
	
}
